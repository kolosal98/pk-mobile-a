package pl.wat.dogweb;


import androidx.appcompat.app.AppCompatActivity;
import androidx.preference.PreferenceManager;

import android.content.SharedPreferences;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import java.util.List;
import java.util.Objects;

import pl.wat.dogweb.models.SharedPrefsDefaults;

import pl.wat.dogweb.models.Dog;
import pl.wat.dogweb.models.StringResponse;
import pl.wat.dogweb.service.NetworkCheck;
import pl.wat.dogweb.service.RetrofitClass;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class EditDogDetails<get> extends AppCompatActivity {

    private EditText mEditDescription;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_edit_dog_details);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        int dogId = getIntent().getIntExtra("dog-id", -1);
        if (dogId == -1) {
            throw new IllegalStateException("DogId nie zostało ustawione!");
        } else {
            SharedPreferences sharedPreferences = PreferenceManager.getDefaultSharedPreferences(this);
            String token = sharedPreferences.getString(SharedPrefsDefaults.TOKEN, null);
            RetrofitClass.getService().getRandomDog(token).enqueue(new Callback<Dog>() {
                @Override
                public void onResponse(Call<Dog> call, Response<Dog> response) {
                    Dog dog = response.body();
                    // TODO - mam psa - działam na nim

                    if (new NetworkCheck(getApplicationContext()).isNetworkAvailable()) {  // TODO - wysyłanie id do serwera żeby odebrać obiekt psa new RetrofitClass
                        String dogId = Objects.requireNonNull(dog.getDogId());
                    }

                    mEditDescription = findViewById(R.id.editText_description);
                    mEditDescription.setText(dog.getDescription(), TextView.BufferType.EDITABLE);
                }

                @Override
                public void onFailure(Call<Dog> call, Throwable t) {
                    // TODO coś nie pykło
                }
            });
        }
    }



    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.edit_dog_menu, menu);
        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                finish();
                return true;
            case R.id.edit_save:
                int dogId = getIntent().getIntExtra("dog-id", -1);
                if (dogId == -1) {
                    throw new IllegalStateException("DogId nie zostało ustawione!");
                } else {
                    SharedPreferences sharedPreferences = PreferenceManager.getDefaultSharedPreferences(this);
                    String token = sharedPreferences.getString(SharedPrefsDefaults.TOKEN, null);
                    RetrofitClass.getService().getRandomDog(token).enqueue(new Callback<Dog>() {
                        @Override
                        public void onResponse(Call<Dog> call, Response<Dog> response) {
                            Dog dog = response.body();
                            // TODO - mam psa - działam na nim

                            mEditDescription = findViewById(R.id.editText_description);
                            mEditDescription.setText(dog.getDescription(), TextView.BufferType.EDITABLE);
                        }

                        @Override
                        public void onFailure(Call<Dog> call, Throwable t) {
                            // TODO coś nie pykło
                        }
                    });
                }

                Toast.makeText(this, "Zapisano", Toast.LENGTH_SHORT).show();
                return true;
        }
        return super.onOptionsItemSelected(item);
    }
}