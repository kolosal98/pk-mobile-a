package pl.wat.dogweb;

import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.view.View;
import android.widget.Toast;

import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import androidx.preference.PreferenceManager;

import pl.wat.dogweb.models.SharedPrefsDefaults;

public class MainActivity extends AppCompatActivity {

    // Wyniesione do klasy SharedPrefsDefaults, żeby ułatwić edycje globalną od SharedPrefs
    // SHAREDPREFS
    public static final String GUEST_MODE = SharedPrefsDefaults.GUEST_MODE;
    public static final String REMEMBER_ME = SharedPrefsDefaults.REMEMBER_ME;
    public static final String TOKEN = SharedPrefsDefaults.TOKEN;

    private SharedPreferences sharedPreferences;
    private SharedPreferences.Editor editor;
    // SHAREDPREFS


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

     // Konieczne dla działania SharedPreferences!!

        sharedPreferences = PreferenceManager.getDefaultSharedPreferences(this);

        editor = sharedPreferences.edit();

        if (sharedPreferences.getBoolean(REMEMBER_ME, false)) {
            finish();
            startActivity(new Intent(this, HomeScreen.class));
        }


    }

    public void ToLoginScreen(View view) {
        if (sharedPreferences.getBoolean(REMEMBER_ME, false)) {
            editor
                    .putBoolean(GUEST_MODE, false)
                    .apply();

            startActivity(new Intent(this, HomeScreen.class));

        } else {

            startActivity(new Intent(this, LoginScreen.class));

        }
    }

    public void GuestMode(View view) {
        boolean temp1;

        if (sharedPreferences.getBoolean(GUEST_MODE, true)) {

            editor
                    .putBoolean(GUEST_MODE, true)
                    .apply();

            startActivity(new Intent(this, HomeScreen.class));

        } else {
            new AlertDialog
                    .Builder(this)
                    .setTitle(R.string.warning)
                    .setMessage(R.string.no_logout_warn)
                    .setPositiveButton("Anuluj", (dialog, which) -> {
                    })
                    .setNegativeButton(R.string.log_out, new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            editor
                                    .putBoolean(GUEST_MODE,true)
                                    .putBoolean(REMEMBER_ME, false)
                                    .putString(TOKEN, null)
                                    .apply();

                            Toast.makeText(MainActivity.this, "Pomyślnie wylogowano", Toast.LENGTH_SHORT).show();
                            startActivity(new Intent(MainActivity.this, HomeScreen.class));
                        }
                    })
                    .setIcon(android.R.drawable.ic_lock_idle_lock)
                    .show();

        }
    }
}
