package pl.wat.dogweb;

import android.content.SharedPreferences;
import android.os.Bundle;
import android.view.View;
import android.widget.EditText;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;
import androidx.preference.PreferenceManager;

import com.google.android.material.textfield.TextInputLayout;

import java.util.Objects;
import java.util.concurrent.atomic.AtomicBoolean;

import pl.wat.dogweb.models.SharedPrefsDefaults;
import pl.wat.dogweb.models.StringResponse;
import pl.wat.dogweb.models.UpdateContactRequest;
import pl.wat.dogweb.models.UpdateDescriptionRequest;
import pl.wat.dogweb.service.RetrofitClass;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class ShelterDescriptionScreen extends AppCompatActivity {

    AtomicBoolean descriptionSuccess = new AtomicBoolean(false);


    public static final String TOKEN = SharedPrefsDefaults.TOKEN;

    private SharedPreferences sharedPreferences;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_admin_shelter_description);
        Objects.requireNonNull(getSupportActionBar()).setDisplayHomeAsUpEnabled(true);

    }
    public void UpdateDescription(View view){
        EditText name_check = findViewById(R.id.shelter_description_name);
        EditText description_check = findViewById(R.id.shelter_description_description);
        EditText nip_check = findViewById(R.id.shelter_description_nip);

        if(checkIfEmptyDescription(name_check,description_check,nip_check)){
            Long shelterId= 1L;
            String name_txt = name_check.getText().toString();
            String description_txt = description_check.getText().toString();
            String nip_txt = nip_check.getText().toString();
            // ???????????????????????????????????????????????????????????????????????????????????????????????????????????????????

            UpdateDescriptionRequest updateDescriptionRequest = new UpdateDescriptionRequest(shelterId,name_txt,description_txt,nip_txt);

            new RetrofitClass().getService().updateDescriptionRequest(PreferenceManager.getDefaultSharedPreferences(this).getString(SharedPrefsDefaults.TOKEN, null),updateDescriptionRequest).enqueue(new Callback<StringResponse>() {
                @Override
                public void onResponse(Call<StringResponse> call, Response<StringResponse> response) {
                    if(response.body().getResp().equals("????????????????????????????????????????????????????"))
                        Toast.makeText(ShelterDescriptionScreen.this, response.body().getResp(), Toast.LENGTH_LONG).show();
                    else
                        Toast.makeText(ShelterDescriptionScreen.this, response.body().getResp(), Toast.LENGTH_LONG).show();
                }



                @Override
                public void onFailure(Call<StringResponse> call, Throwable t) {
                    Toast.makeText(ShelterDescriptionScreen.this, "Error", Toast.LENGTH_LONG).show();
                }
            });
        }
    }


    // Funkcja sprawdzająca czy został wpisany nazwa, opis i numer NIP
    public boolean checkIfEmptyDescription(EditText name_check,
                                EditText description_check,
                                EditText nip_check
    ) {

        boolean corr1, corr2,corr3;

        // Sprawdź czy puste
        boolean name = name_check.getText().toString().isEmpty();
        boolean description = description_check.getText().toString().isEmpty();
        boolean nip = nip_check.getText().toString().isEmpty();


        TextInputLayout name_check_parent = findViewById(R.id.shelter_description_name_parent);
        TextInputLayout description_check_parent = findViewById(R.id.shelter_description_description_parent);
        TextInputLayout nip_check_parent =findViewById(R.id.shelter_description_nip_parent);


        // Weryfikacja nazwy
        if (name) {
            name_check_parent.setError(getString(R.string.remail_cant_be_empty));
            corr1 = false;
        } else corr1 = true;

        // Weryfikacja opisu
        if (description) {
            description_check_parent.setError(getString(R.string.password_cant_be_empty));
            corr2 = false;
        } else corr2 = true;

        // Weryfikacja NIPu
        if (nip) {
            nip_check_parent.setError(getString(R.string.password_cant_be_empty));
            corr3 = false;
        } else corr3 = true;


        if (corr1 && corr2 && corr3) {
            descriptionSuccess.set(true);
        } else descriptionSuccess.set(false);

        return descriptionSuccess.get();
    }

}