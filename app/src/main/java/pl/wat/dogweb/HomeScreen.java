package pl.wat.dogweb;

import android.content.Intent;
import android.content.SharedPreferences;
import android.net.Uri;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import androidx.navigation.NavController;
import androidx.navigation.Navigation;
import androidx.navigation.ui.AppBarConfiguration;
import androidx.navigation.ui.NavigationUI;
import androidx.preference.PreferenceManager;

import com.google.android.material.bottomnavigation.BottomNavigationView;

import pl.wat.dogweb.models.SharedPrefsDefaults;

public class HomeScreen extends AppCompatActivity {

    // SHAREDPREFS
    public static final String GUEST_MODE = SharedPrefsDefaults.GUEST_MODE;
    public static final String REMEMBER_ME = SharedPrefsDefaults.REMEMBER_ME;
    public static final String TOKEN = SharedPrefsDefaults.TOKEN;


    private SharedPreferences sharedPreferences;
    private SharedPreferences.Editor editor;
    // SHAREDPREFS

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_home_screen);
        BottomNavigationView navView = findViewById(R.id.nav_view);
        AppBarConfiguration appBarConfiguration = new AppBarConfiguration.Builder(
                R.id.navigation_dogs, R.id.navigation_donate, R.id.navigation_contact)
                .build();
        NavController navController = Navigation.findNavController(this, R.id.nav_host_fragment);
        NavigationUI.setupActionBarWithNavController(this, navController, appBarConfiguration);
        NavigationUI.setupWithNavController(navView, navController);

        // Konieczne dla działania SharedPreferences!!
        sharedPreferences = PreferenceManager.getDefaultSharedPreferences(this);
        editor = sharedPreferences.edit();

        Button adminButton= findViewById(R.id.adminbutton);
        if (adminButton==null) {

        }else{
            adminButton.setVisibility(View.GONE);
        }
    }

    // <- Lista (wyloguj)

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.main_menu, menu);
        MenuItem item = menu.findItem(R.id.log_out_button);

        // Ukryj wyloguj dla gości
        if(sharedPreferences.getBoolean(GUEST_MODE, true)) {
            item.setVisible(false);
            this.invalidateOptionsMenu();
        }
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(@NonNull MenuItem item) {
        switch (item.getItemId()) {
            case R.id.log_out_button:
                startActivity(new Intent(this, MainActivity.class)
                        .setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_NEW_TASK));
                editor
                        .putBoolean(GUEST_MODE, true)
                        .putBoolean(REMEMBER_ME, false)
                        .apply();
                finish();
                return true;
            case R.id.usr_settings:
                startActivity(new Intent(this, SettingsActivity.class));
                return true;

            default:
                return true;
        }
    }

    // Lista (wyloguj) ->

    @Override
    public void onBackPressed() {
        if (!sharedPreferences.getBoolean(GUEST_MODE,true) && !sharedPreferences.getBoolean(REMEMBER_ME, false)) {

            new AlertDialog
                    .Builder(this)
                    .setTitle(R.string.warning)
                    .setMessage(R.string.are_you_sure_to_log_out)
                    .setPositiveButton(android.R.string.yes, (dialog, which) -> {
                        editor
                                .putBoolean(GUEST_MODE,true)
                                .putString(TOKEN, null)
                                .apply();
                        Toast.makeText(HomeScreen.this, "Jesteś teraz wylogowany", Toast.LENGTH_SHORT).show();

                        // TODO ZROBIĆ FAKTYCZNE WYLOGOWANIE

                        finish();
                    })
                    .setNegativeButton(R.string.cancel, null)
                    .setIcon(android.R.drawable.ic_dialog_alert)
                    .show();

        } else finish();
    }

    public void ContactUs(View view) {

        Intent emailIntent = new Intent(Intent.ACTION_SENDTO);
        emailIntent.setData(Uri.parse("mailto:"));
        emailIntent.putExtra(Intent.EXTRA_SUBJECT, "Subject");
        emailIntent.putExtra(Intent.EXTRA_TEXT, "Message...");

        try {
            startActivity(emailIntent);
            finish();
        } catch (android.content.ActivityNotFoundException ex) {
            Toast.makeText(this,
                    "Brak aplikacji do obsługi poczty!", Toast.LENGTH_SHORT).show();
        }
    }

    public void DogList(View view) {
        startActivity(new Intent(this, DogListActivity.class));
    }
    public void DonateUs (View view) {
        startActivity(new Intent(this, DonateScreen.class));
    }
    public void WatchAd (View view) {
        startActivity(new Intent(this, AdvertScreen.class));
    }
    public void ToAdminPanel(View View){
        startActivity(new Intent(this, AdminPanelScreen.class));
    }


}




