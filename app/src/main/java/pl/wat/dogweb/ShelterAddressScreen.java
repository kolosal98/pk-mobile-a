package pl.wat.dogweb;

import android.content.SharedPreferences;
import android.os.Bundle;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.widget.EditText;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;
import androidx.preference.PreferenceManager;

import com.google.android.material.textfield.TextInputLayout;

import java.util.Objects;
import java.util.concurrent.atomic.AtomicBoolean;

import pl.wat.dogweb.models.SharedPrefsDefaults;
import pl.wat.dogweb.models.StringResponse;
import pl.wat.dogweb.models.UpdateAddressRequest;
import pl.wat.dogweb.service.RetrofitClass;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class ShelterAddressScreen extends AppCompatActivity {

    AtomicBoolean addressSuccess = new AtomicBoolean(false);


    public static final String TOKEN = SharedPrefsDefaults.TOKEN;

    private SharedPreferences sharedPreferences;




    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_admin_shelter_address);
        Objects.requireNonNull(getSupportActionBar()).setDisplayHomeAsUpEnabled(true);

    }


    public void UpdateAddress(View view) {

        EditText city_check = findViewById(R.id.shelter_address_city);
        EditText street_check = findViewById(R.id.shelter_address_street);
        EditText buildingNumber_check = findViewById(R.id.shelter_address_building_number);
        EditText postalCode_check = findViewById(R.id.shelter_address_postal_code);
        EditText province_check = findViewById(R.id.shelter_address_province);

        if (checkIfEmptyAddress(city_check, street_check, buildingNumber_check, postalCode_check, province_check)) {
            Long shelterId = 1L;
            String city_txt = city_check.getText().toString();
            String street_txt = street_check.getText().toString();
            String buildingNumber_txt = buildingNumber_check.getText().toString();
            String postalCode_txt = postalCode_check.getText().toString();
            String province_txt = province_check.getText().toString();


            UpdateAddressRequest updateAddressRequest = new UpdateAddressRequest(
                    shelterId,
                    city_txt,
                    street_txt,
                    buildingNumber_txt,
                    postalCode_txt,
                    province_txt);

            new RetrofitClass().getService().requestUpdateAddress(PreferenceManager.getDefaultSharedPreferences(this).getString(SharedPrefsDefaults.TOKEN, null), updateAddressRequest).enqueue(new Callback<StringResponse>() {
                @Override
                public void onResponse(Call<StringResponse> call, Response<StringResponse> response) {
                    if (response.body().getResp().equals("Zmieniłeś adres schroniska")) {
                        Toast.makeText(ShelterAddressScreen.this, response.body().getResp(), Toast.LENGTH_LONG).show();
                    } else {
                        Toast.makeText(ShelterAddressScreen.this, "Error", Toast.LENGTH_LONG).show();
                    }


                }

                @Override
                public void onFailure(Call<StringResponse> call, Throwable t) {

                    Toast.makeText(ShelterAddressScreen.this, call.toString(), Toast.LENGTH_SHORT).show();
                    Log.d("SERWER RESPONSE: ", "Error", t);


                }
            });
        }
    }




    // Funkcja sprawdzająca czy został wpisany email, hasło i potwierdzenie hasła
    public boolean checkIfEmptyAddress(EditText city_check,
                                EditText street_check,
                                EditText buildingNumber_check,
                                EditText postalCode_check,
                                EditText province_check) {

        boolean corr1, corr2,corr3,corr4,corr5;

        // Sprawdź czy puste
        boolean city = city_check.getText().toString().isEmpty();
        boolean street = street_check.getText().toString().isEmpty();
        boolean buildingNumber =buildingNumber_check.getText().toString().isEmpty();
        boolean postalCode = postalCode_check.getText().toString().isEmpty();
        boolean province = province_check.getText().toString().isEmpty();

        TextInputLayout city_check_parent = findViewById(R.id.shelter_address_city_parent);
        TextInputLayout street_check_parent = findViewById(R.id.shelter_address_street_parent);
        TextInputLayout build_check_parent =findViewById(R.id.shelter_address_building_number_parent);
        TextInputLayout postalCode_check_parent = findViewById(R.id.shelter_address_postal_code_parent);
        TextInputLayout province_check_parent = findViewById(R.id.shelter_address_province_parent);

        // Weryfikacja loginu
        if (city) {
            city_check_parent.setError(getString(R.string.password_cant_be_empty));
            corr1 = false;
        } else corr1 = true;

        // Weryfikacja hasła
        if (street) {
            street_check_parent.setError(getString(R.string.password_cant_be_empty));
            corr2 = false;
        } else corr2 = true;

        // Weryfikacja potwierdzenia hasła
        if (buildingNumber) {
            build_check_parent.setError(getString(R.string.password_cant_be_empty));
            corr3 = false;
        } else corr3 = true;

        // Weryfikacja potwierdzenia hasła
        if (postalCode) {
            postalCode_check_parent.setError(getString(R.string.password_cant_be_empty));
            corr4 = false;
        } else corr4 = true;

        // Weryfikacja potwierdzenia hasła
        if (province) {
            province_check_parent.setError(getString(R.string.password_cant_be_empty));
            corr5 = false;
        } else corr5 = true;

        if (corr1 && corr2 && corr3 && corr4 && corr5) {
            addressSuccess.set(true);
        } else addressSuccess.set(false);

        return addressSuccess.get();
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                finish();
                return true;
        }
        return super.onOptionsItemSelected(item);
    }

}
