package pl.wat.dogweb.ui.home_dogs;

import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;
import androidx.lifecycle.ViewModel;

public class DogsViewModel extends ViewModel {

    private MutableLiveData<String> mText;

    public DogsViewModel() {
        mText = new MutableLiveData<>();
    }

    public LiveData<String> getText() {
        return mText;
    }
}