package pl.wat.dogweb.ui.home_dogs;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProvider;

import pl.wat.dogweb.R;

public class DogsFragment extends Fragment {

    private DogsViewModel dogsViewModel;

    public View onCreateView(@NonNull LayoutInflater inflater,
                             ViewGroup container, Bundle savedInstanceState) {
        dogsViewModel =
                new ViewModelProvider(this).get(DogsViewModel.class);
        View root = inflater.inflate(R.layout.fragment_dogs, container, false);

        dogsViewModel.getText().observe(getViewLifecycleOwner(), new Observer<String>() {
            @Override
            public void onChanged(@Nullable String s) {

            }
        });
        return root;
    }

}