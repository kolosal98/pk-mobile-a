package pl.wat.dogweb;

//LAYOUT DO ZROBIENIA

import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;
import androidx.preference.PreferenceManager;

import com.google.android.material.textfield.TextInputLayout;

import java.util.concurrent.atomic.AtomicBoolean;

import pl.wat.dogweb.models.AlertBox;
import pl.wat.dogweb.models.LoginRequest;
import pl.wat.dogweb.models.LoginResponse;
import pl.wat.dogweb.models.SharedPrefsDefaults;
import pl.wat.dogweb.service.NetworkCheck;
import pl.wat.dogweb.service.RetrofitClass;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class LoginScreen extends AppCompatActivity {


    // SHAREDPREFS
    public static final String GUEST_MODE = SharedPrefsDefaults.GUEST_MODE;
    public static final String LOGIN = SharedPrefsDefaults.LOGIN;
    public static final String PASSWORD = SharedPrefsDefaults.PASSWORD;
    public static final String REMEMBER_ME = SharedPrefsDefaults.REMEMBER_ME;
    public static final String TOKEN = SharedPrefsDefaults.TOKEN;

    private SharedPreferences sharedPreferences;
    private SharedPreferences.Editor editor;
    // SHAREDPREFS

    static AtomicBoolean loginSuccess = new AtomicBoolean(false);

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login_screen);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        // Konieczne dla działania SharedPreferences!!
        sharedPreferences = PreferenceManager.getDefaultSharedPreferences(this);
        editor = sharedPreferences.edit();

    }

    public void ToHomeScreen(View view) {

        EditText login_check = findViewById(R.id.login_here);
        EditText password_check = findViewById(R.id.password_here);

        //TODO przy niewpisaniu hasła pojawia się ikonka "brak hasła" zasłaniająca toggle do hasła

        if (checkLoginPassword(login_check, password_check)) {

            String login_txt = login_check.getText().toString();
            String password_txt = password_check.getText().toString();

            LoginRequest loginRequest = new LoginRequest(login_txt,password_txt);

            new RetrofitClass().getService().requestAuth(loginRequest).enqueue(new Callback<LoginResponse>() {
                @Override
                public void onResponse(Call<LoginResponse> call, Response<LoginResponse> response) {
                    if (response.body() != null && response.isSuccessful()) {
                        
                        CheckBox checkBox = findViewById(R.id.checkBox_remember_logged_user);
                        if (checkBox.isChecked()) {
                            editor
                                    .putString(LOGIN, login_txt)
                                    .putBoolean(REMEMBER_ME, true)
                                    .apply();
                        } else {
                            editor
                                    .remove(LOGIN)
                                    .remove(PASSWORD)
                                    .putBoolean(REMEMBER_ME, false)
                                    .apply();
                        }

                        editor
                                .putBoolean(GUEST_MODE, false)
                                .putString(TOKEN, response.body().getJwt())
                                .apply();

                        startActivity(new Intent(LoginScreen.this, HomeScreen.class).setFlags(Intent.FLAG_ACTIVITY_SINGLE_TOP));
                        finish();
                    } else {
                        Toast.makeText(LoginScreen.this, "Incorrect login", Toast.LENGTH_SHORT).show();
                    }
                }

                @Override
                public void onFailure(Call<LoginResponse> call, Throwable t) {
                    Toast.makeText(LoginScreen.this, "No communication with server", Toast.LENGTH_SHORT).show();
                    Log.d("SERWER RESPONSE: ", call.toString(), t);
                }
            });
        }
    }

    // Strzałka cofania
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                finish();
                return true;
        }
        return super.onOptionsItemSelected(item);
    }


    // Funkcja sprawdzająca login, hasło i połączenie z internetem - użyta w ToHomeScreen
    public boolean checkLoginPassword(EditText login_check, EditText password_check) {

        boolean corr1;
        boolean corr2;
        boolean corr3;

        // Sprawdź czy puste
        boolean login = login_check.getText().toString().isEmpty();
        boolean password = password_check.getText().toString().isEmpty();

        TextInputLayout password_check_parent = findViewById(R.id.password_parent);
        TextInputLayout login_check_parent = findViewById(R.id.login_parent);

        // Weryfikacja loginu
        if (login) {
            login_check_parent.setError(getString(R.string.login_cant_be_empty));
            corr1 = false;
        } else corr1 = true;

        // Weryfikacja hasła
        if (password) {
            password_check_parent.setError(getString(R.string.password_cant_be_empty));
            corr2 = false;
        } else corr2 = true;

        // Weryfikacja połączenia z internetem
        if (new NetworkCheck(getApplicationContext()).isNetworkAvailable()) {
            corr3 = true;
        } else {
            corr3 = false;
            new AlertBox(LoginScreen.this, R.string.no_connection_login).noWifi();
        }

        if (corr1 && corr2 && corr3) {

            editor.putBoolean(GUEST_MODE, false).apply();
            loginSuccess.set(true);

        } else loginSuccess.set(false);

        return loginSuccess.get();
    }
    public void ToRegistration(View View){
        startActivity(new Intent(this, RegistrationScreen.class));
    }
}
