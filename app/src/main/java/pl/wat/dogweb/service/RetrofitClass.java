package pl.wat.dogweb.service;

import okhttp3.OkHttpClient;
import okhttp3.logging.HttpLoggingInterceptor;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class RetrofitClass {

    private static RetrofitInterface service = null;

    public static RetrofitInterface getService() {
        if (service == null) {
            final HttpLoggingInterceptor interceptor = new HttpLoggingInterceptor();
            final OkHttpClient client = new OkHttpClient.Builder().addInterceptor(interceptor).build();
            interceptor.setLevel(HttpLoggingInterceptor.Level.BODY);

            final Retrofit retrofit = new Retrofit.Builder()
                    .baseUrl("http://10.2.7.125:8080/")
                    .addConverterFactory(GsonConverterFactory.create())
                    .client(client)
                    .build();

            service = retrofit.create(RetrofitInterface.class);
        } return service;
    }
}
