package pl.wat.dogweb;

import android.annotation.SuppressLint;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.preference.PreferenceManager;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.google.android.material.internal.NavigationMenu;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;
import java.util.Objects;

import io.github.yavski.fabspeeddial.FabSpeedDial;
import pl.wat.dogweb.adapters.DogListAdapter;
import pl.wat.dogweb.models.AlertBox;
import pl.wat.dogweb.models.Dog;
import pl.wat.dogweb.models.SharedPrefsDefaults;
import pl.wat.dogweb.models.Singletone;
import pl.wat.dogweb.service.NetworkCheck;
import pl.wat.dogweb.service.RetrofitClass;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;


public class DogListActivity extends AppCompatActivity {

    // SHAREDPREFS
    public static final String GUEST_MODE = SharedPrefsDefaults.GUEST_MODE;
    public static final String TOKEN = SharedPrefsDefaults.TOKEN;

    private SharedPreferences sharedPreferences;
    // SHAREDPREFS

    public List<Dog> dogsList = new ArrayList<>();

    @SuppressLint("NotifyDataSetChanged")
    private void sortName() {
        Collections.sort(dogsList, new Comparator<Dog>() {
            @Override
            public int compare(Dog o1, Dog o2) {
                return o1.getName().compareTo(o2.getName());
            }
        });
        setAdapter(dogsList);
    }

    @SuppressLint("NotifyDataSetChanged")
    private void sortRace() {
        Collections.sort(dogsList, new Comparator<Dog>() {
            @Override
            public int compare(Dog o1, Dog o2) {
                return o1.getRace().compareTo(o2.getRace());
            }
        });
        setAdapter(dogsList);
    }

    @SuppressLint("NotifyDataSetChanged")
    private void sortDate() {
        Collections.sort(dogsList, new Comparator<Dog>() {
            @Override
            public int compare(Dog o1, Dog o2) {
                return o1.getAddedDate().compareTo(o2.getAddedDate());
            }
        });
        setAdapter(dogsList);
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_dog_list);
        Objects.requireNonNull(getSupportActionBar()).setDisplayHomeAsUpEnabled(true);

        // Konieczne dla działania SharedPreferences!!
        sharedPreferences = PreferenceManager.getDefaultSharedPreferences(this);

        // Fabmenu
        FabSpeedDial fabSpeedDial = findViewById(R.id.fabSpeedDial);
        fabSpeedDial.setMenuListener(new FabSpeedDial.MenuListener() {
            @Override
            public boolean onPrepareMenu(NavigationMenu navigationMenu) {
                return true;
            }

            @Override
            public boolean onMenuItemSelected(MenuItem menuItem) {

                switch (menuItem.getItemId()) {
                    case R.id.fab_action_1:
                        if (new NetworkCheck(getApplicationContext()).isNetworkAvailable()) {

                            //TODO Start activity for result -> DogAddActivity ma zwracać true jesli zostanie dodany pies i aktualizować listę.
                            startActivity(new Intent(DogListActivity.this, DogAddActivity.class));
                        } else {
                            new AlertBox(DogListActivity.this, R.string.no_connection_add_dog).noWifi();
                        }
                        return true;

                    case R.id.fab_action_2:

                        startActivity(new Intent(DogListActivity.this, InfoScreen.class));
                        return true;

                    case R.id.fab_action_3:

                        Toast.makeText(DogListActivity.this, "Opcja dostępna wkrótce ツ", Toast.LENGTH_SHORT).show();
                        return true;

                    default:
                        return true;
                }
            }

            @Override
            public void onMenuClosed() {

            }
        });


        // Ukrywanie pływającego przycisku dla gości
        if (sharedPreferences.getBoolean(GUEST_MODE, true)) {
            fabSpeedDial.setVisibility(View.INVISIBLE);
        }

        // Ustawiam RecyclerView i adapter
        new RetrofitClass().getService().getAllDogs(sharedPreferences.getString(TOKEN, null)).enqueue(new Callback<List<Dog>>() {
    // TODO zmienić obsługę z retrofit na okhttp (interceptor)
            @Override
            public void onResponse(Call<List<Dog>> call, Response<List<Dog>> response) {

                if (response.isSuccessful()) {
                    dogsList = response.body();
                    setAdapter(dogsList);

                    Toast.makeText(DogListActivity.this, "Odczytano listę!", Toast.LENGTH_SHORT).show();
                } else {
                    Toast.makeText(DogListActivity.this, response.toString(), Toast.LENGTH_LONG).show();
                }

            }

            @Override
            public void onFailure(Call<List<Dog>> call, Throwable t) {
                Log.w("getDogList_ERROR", t);
                Toast.makeText(DogListActivity.this, "Nie połączono z serwerem", Toast.LENGTH_SHORT).show();
            }
        });
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.filter_menu, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(@NonNull MenuItem item) {
        switch (item.getItemId()) {

            case R.id.search:
                Toast.makeText(DogListActivity.this, "Wyszukiwanie", Toast.LENGTH_SHORT).show();
                return true;

            case R.id.filter_name:
                sortName();
                return true;

            case R.id.filter_race:
                sortRace();
                return true;

            case R.id.filter_date:
                sortDate();
                return true;

            case android.R.id.home:
                finish();

            default:
                return true;
        }
    }

    public void setAdapter(List<Dog> dogsList) {
        if (dogsList != null) {
            DogListAdapter dogListAdapter = new DogListAdapter(dogsList);
            RecyclerView dogRecyclerView = findViewById(R.id.dogs_list_recycler_view);
            dogRecyclerView.setLayoutManager(new LinearLayoutManager(this));
            dogRecyclerView.setAdapter(dogListAdapter);

            dogListAdapter.setOnItemClickListener(new DogListAdapter.OnItemClickListener() {
                @Override
                public void onItemClick(int position) {
                    goToDetail(position);
                    Singletone.setSelectedDog(dogsList.get(position));
                }
            });
        }
    }

    public void goToDetail(int position) {
        int dogId = dogsList.get(position).getId();
        Intent intent = new Intent(this, InfoScreen.class);
        intent.putExtra("dog-id", dogId);
        startActivity(intent);
    }
}
