package pl.wat.dogweb;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;


import android.os.Bundle;

import java.util.ArrayList;

import pl.wat.dogweb.adapters.BigImageSliderAdapter;

public class BigSlider extends AppCompatActivity {

    private ArrayList<String> imageUrls = new ArrayList<>();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_big_slider);

        getImages();
    }


    private void getImages(){

        imageUrls.add("https://schronisko.com/wp-content/uploads/2021/05/1.jpeg");
        imageUrls.add("https://schronisko.com/wp-content/uploads/2021/05/2.jpeg");
        imageUrls.add("https://schronisko.com/wp-content/uploads/2021/05/3.jpeg");
        imageUrls.add("https://schronisko.com/wp-content/uploads/2021/05/4.jpeg");
        imageUrls.add("https://schronisko.com/wp-content/uploads/2021/05/5.jpeg");
        imageUrls.add("https://schronisko.com/wp-content/uploads/2021/05/6.jpeg");

        initRecycleView();
    }

    private void initRecycleView(){

        LinearLayoutManager layoutManager = new LinearLayoutManager(this, LinearLayoutManager.HORIZONTAL, false);
        RecyclerView recyclerView = findViewById(R.id.recycler_view_big_slider);
        recyclerView.setLayoutManager(layoutManager);
        BigImageSliderAdapter imageSliderAdapter = new BigImageSliderAdapter(imageUrls, this);
        recyclerView.setAdapter(imageSliderAdapter);
    }

}

