package pl.wat.dogweb.adapters;

import android.annotation.SuppressLint;
import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;

import java.util.ArrayList;

import pl.wat.dogweb.R;

public class BigImageSliderAdapter extends RecyclerView.Adapter<BigImageSliderAdapter.ViewHolder> {


    private ArrayList<String> mImageUrls = new ArrayList<>();
    private Context mContext;


    public BigImageSliderAdapter(ArrayList<String> imageUrls, Context context) {
        mImageUrls = imageUrls;
        mContext = context;
    }

    @NonNull
    @Override
    public BigImageSliderAdapter.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {

        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.big_image_slider_element, parent, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull BigImageSliderAdapter.ViewHolder holder, @SuppressLint("RecyclerView") final int position) {

        Glide.with(mContext)
                .load(mImageUrls.get(position))
                .centerCrop()
                .fitCenter()
                .into(holder.imageView);


        holder.imageView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(Intent.ACTION_VIEW).setData(Uri.parse(mImageUrls.get(position)));
                mContext.startActivity(intent);
                Toast.makeText(mContext, mImageUrls.get(position), Toast.LENGTH_SHORT).show();
            }
        });
    }

    @Override
    public int getItemCount() {
        return mImageUrls.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {

        ImageView imageView;

        public ViewHolder(@NonNull View itemView) {
            super(itemView);

            imageView = itemView.findViewById(R.id.image_element);
        }
    }
}
