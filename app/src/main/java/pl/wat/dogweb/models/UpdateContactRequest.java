package pl.wat.dogweb.models;

public class UpdateContactRequest {

        Long id;
        String email;
        String wwwPage;
        String phone;

    public UpdateContactRequest(){

    }

    public UpdateContactRequest(Long id, String email, String wwwPage, String phone){
        this.id=id;
        this.email=email;
        this.wwwPage=wwwPage;
        this.phone=phone;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public String getWwwPage() {
        return wwwPage;
    }

    public void setWwwPage(String wwwPage) {
        this.wwwPage = wwwPage;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }
}
