package pl.wat.dogweb.models;

public class Dog {

    // ID ze schroniska
    boolean checkedOut;
    String description;
    String name;
    DogRace race;
    String addedDate;
    String dogId;
    int shelterID;
    int dogWebUserId;
    int id;
    String imageDogURL;
    String unregisteredDate;

    public Dog() {
    }

    public Dog(String addedDate, boolean checkedOut, String description, String dogId, int dogWebUserId, int id, String imageDogURL, String name, DogRace race, int shelterID, String unregisteredDate) {
        this.checkedOut = checkedOut;
        this.description = description;
        this.name = name;
        this.race = race;
        this.addedDate = addedDate;
        this.dogId = dogId;
        this.shelterID = shelterID;
        this.dogWebUserId = dogWebUserId;
        this.id = id;
        this.imageDogURL = imageDogURL;
        this.unregisteredDate = unregisteredDate;
    }

    public boolean isCheckedOut() {
        return checkedOut;
    }

    public void setCheckedOut(boolean checkedOut) {
        this.checkedOut = checkedOut;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public DogRace getRace() {
        return race;
    }

    public void setRace(DogRace race) {
        this.race = race;
    }

    public String getAddedDate() {
        return addedDate;
    }

    public void setAddedDate(String addedDate) {
        this.addedDate = addedDate;
    }

    public String getDogId() {
        return dogId;
    }

    public void setDogId(String dogId) {
        this.dogId = dogId;
    }

    public int getShelterID() {
        return shelterID;
    }

    public void setShelterID(int shelterID) {
        this.shelterID = shelterID;
    }

    public int getDogWebUserId() {
        return dogWebUserId;
    }

    public void setDogWebUserId(int dogWebUserId) {
        this.dogWebUserId = dogWebUserId;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getImageDogURL() {
        return imageDogURL;
    }

    public void setImageDogURL(String imageDogURL) {
        this.imageDogURL = imageDogURL;
    }

    public String getUnregisteredDate() {
        return unregisteredDate;
    }

    public void setUnregisteredDate(String unregisteredDate) {
        this.unregisteredDate = unregisteredDate;
    }

    @Override
    public String toString() {
        return "Dog{" +
                "dogId='" + dogId + '\'' +
                ", name='" + name + '\'' +
                ", race=" + race +
                ", date='" + addedDate + '\'' +
                '}';
    }
}
