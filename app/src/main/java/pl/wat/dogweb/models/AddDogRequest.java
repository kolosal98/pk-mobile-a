package pl.wat.dogweb.models;

public class AddDogRequest {

    boolean checkedOut;
    String dogId;
    String description;
    String imageDogUrl;
    String name;
    String race;
    int shelterId;
    int dogWebUserID;

    public AddDogRequest(boolean checkedOut, String description, String dogId, int dogWebUserID, String imageDogUrl, String name, String race, int shelterId) {
        this.checkedOut = checkedOut;
        this.dogId = dogId;
        this.description = description;
        this.imageDogUrl = imageDogUrl;
        this.name = name;
        this.race = race;
        this.shelterId = shelterId;
        this.dogWebUserID = dogWebUserID;
    }

    public boolean isCheckedOut() {
        return checkedOut;
    }

    public void setCheckedOut(boolean checkedOut) {
        this.checkedOut = checkedOut;
    }

    public String getDogId() {
        return dogId;
    }

    public void setDogId(String dogId) {
        this.dogId = dogId;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getImageDogUrl() {
        return imageDogUrl;
    }

    public void setImageDogUrl(String imageDogUrl) {
        this.imageDogUrl = imageDogUrl;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getRace() {
        return race;
    }

    public void setRace(String race) {
        this.race = race;
    }

    public int getShelterId() {
        return shelterId;
    }

    public void setShelterId(int shelterId) {
        this.shelterId = shelterId;
    }

    public int getDogWebUserID() {
        return dogWebUserID;
    }

    public void setDogWebUserID(int dogWebUserID) {
        this.dogWebUserID = dogWebUserID;
    }
}
