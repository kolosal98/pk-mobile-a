package pl.wat.dogweb.models;

public enum Roles {

    ROLE_ADMIN,
    ROLE_EMPLOYEE,
    ROLE_SUPER_ADMIN,
    ROLE_USER

}
