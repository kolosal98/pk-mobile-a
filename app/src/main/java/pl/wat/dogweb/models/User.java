package pl.wat.dogweb.models;

public class User {

    String email;
    String password;
    Roles role;

    Integer shelterID;

    public User() {

    }

    public User(String email, String password, Roles role, Integer shelterID) {
        this.email = email;
        this.password = password;
        this.role = role;
        this.shelterID = shelterID;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public Roles getRole() {
        return role;
    }

    public void setRole(Roles role) {
        this.role = role;
    }

    public Integer getShelterID() {
        return shelterID;
    }

    public void setShelterID(Integer shelterID) {
        this.shelterID = shelterID;
    }
}
