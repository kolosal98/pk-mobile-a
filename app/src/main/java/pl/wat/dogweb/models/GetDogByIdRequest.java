package pl.wat.dogweb.models;

public class GetDogByIdRequest {
    String dogId;

    public GetDogByIdRequest(String dogId) {
        this.dogId = dogId;
    }

    public String getDogId() {
        return dogId;
    }

    public void setDogId(String dogId) {
        this.dogId = dogId;
    }
}
