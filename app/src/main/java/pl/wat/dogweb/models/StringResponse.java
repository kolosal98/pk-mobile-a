package pl.wat.dogweb.models;

public class StringResponse {
    String resp;

    public StringResponse(){

    }

    public StringResponse(String resp){
        this.resp=resp;
    }

    public String getResp() {
        return resp;
    }

    public void setResp(String resp) {
        this.resp = resp;
    }
}
