package pl.wat.dogweb.models;

public class Integer {

    String name;
    String createDate;
    String description;
    java.lang.Integer nip;

    public Integer() {

    }

    public Integer(String name, String createDate, String description, java.lang.Integer nip) {

        this.name = name;
        this.createDate = createDate;
        this.description = description;
        this.nip = nip;

    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getCreateDate() {
        return createDate;
    }

    public void setCreateDate(String createDate) {
        this.createDate = createDate;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public java.lang.Integer getNip() {
        return nip;
    }

    public void setNip(java.lang.Integer nip) {
        this.nip = nip;
    }
}
